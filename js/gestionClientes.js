var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
var flagUrl = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

function getClientes() {
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if(this.readyState==4 || this.status==200) {
      //console.table(JSON.parse(request.responseText).value);
      let clientesObtenidos = request.responseText;
      procesarClientes(clientesObtenidos);
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarClientes(clientes) {
  let jsonClientes = JSON.parse(clientes);
  let divTabla = document.getElementById("divClientes");
  let tabla = document.createElement("table");
  let tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-hover");

  for(let i=0; i<jsonClientes.value.length; i++) {
    //console.log(jsonClientes.value[i].ProductName);
    let nuevaFila = document.createElement("tr");
    let columnaContact = document.createElement("td");
    columnaContact.innerText = jsonClientes.value[i].ContactName;

    let columnaCity = document.createElement("td");
    columnaCity.innerText = jsonClientes.value[i].City;

    let columnaCountry = document.createElement("td");
    columnaCountry.innerText = jsonClientes.value[i].Country;
    let imgFlag = document.createElement("img");
    imgFlag.classList.add("flag");
    let pais = jsonClientes.value[i].Country;
    if(pais=="UK") {
      pais = "United-Kingdom";
    }
    imgFlag.src = flagUrl+pais+".png";
    nuevaFila.appendChild(columnaContact);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(imgFlag);
    nuevaFila.appendChild(columnaCountry);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}

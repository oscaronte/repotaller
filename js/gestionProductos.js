function getProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if(this.readyState==4 || this.status==200) {
      //console.table(JSON.parse(request.responseText).value);
      let productosObtenidos = request.responseText;
      procesarProductos(productosObtenidos);
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarProductos(productos) {
  let jsonProductos = JSON.parse(productos);
  let divTabla = document.getElementById("divTabla");
  let tabla = document.createElement("table");
  let tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-hover");

  for(let i=0; i<jsonProductos.value.length; i++) {
    //console.log(jsonProductos.value[i].ProductName);
    let nuevaFila = document.createElement("tr");
    let columnaNombre = document.createElement("td");
    columnaNombre.innerText = jsonProductos.value[i].ProductName;

    let columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = jsonProductos.value[i].UnitPrice;

    let columnaStock = document.createElement("td");
    columnaStock.innerText = jsonProductos.value[i].UnitsInStock;
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
